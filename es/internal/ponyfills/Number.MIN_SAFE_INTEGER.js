var MIN_SAFE_INTEGER = Number.MIN_SAFE_INTEGER || -Math.pow(2, 53) - 1;
export default MIN_SAFE_INTEGER;